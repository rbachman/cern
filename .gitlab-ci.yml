stages:
  - build
  - version-check
  - deploy

before_script:
  - mkdir -p .repo

build:
  stage: build
  image: gitlab-registry.cern.ch/cloud/ciadm
  script:
    - curl -o helm.tar.gz https://kubernetes-helm.storage.googleapis.com/helm-v2.14.1-linux-amd64.tar.gz; mkdir -p helm; tar zxvf helm.tar.gz -C helm; cp helm/linux-amd64/helm /usr/local/bin; rm -rf helm*
    - helm init --client-only
    - for chart in $(ls -d */Chart.yaml | xargs dirname); do helm dep update ${chart}; helm lint ${chart}; helm package ${chart}; done
  except:
    - tags

version-check:
  stage: version-check
  dependencies:
    - build
  image: gitlab-registry.cern.ch/cloud/ciadm
  script:
    - |
        # Get all diff folders
        CHART=$(git diff-tree --name-only --no-commit-id HEAD origin/master)
        # Remove non chart files
        for base in $(find . -maxdepth 1 -type f | sed 's|./||' | xargs); do
            CHART=$(echo ${CHART} | sed "s|${base}||")
        done
        if [ "$(echo ${CHART} | wc -w)" = 0 ]; then
            echo "Changes do not affect charts. skipping"
            exit 0;
        elif [ "$(echo ${CHART} | wc -w)" != 1 ]; then
            echo "ERROR: You can only merge changes on one chart. Please fix before merging again."
            exit 1;
        else
            OLD_CHART_VERSION="$(echo "$(git diff origin/master -- $CHART/Chart.yaml)" | grep "\-version:" | awk '{print $2}')"
            NEW_CHART_VERSION="$(echo "$(git diff origin/master -- $CHART/Chart.yaml)" | grep "+version:" | awk '{print $2}')"
        fi
    - |
        # Check if Chart version was bumped
        if [ ${NEW_CHART_VERSION} = "" ] || \
            [ $(expr ${NEW_CHART_VERSION} \<= ${OLD_CHART_VERSION}) -eq 1 ]; then
            echo "ERROR: Chart version must be higher than existent. Please fix before merging again."
            exit 1
        fi
  except:
    - tags
  only:
    - master

deploy:
  stage: deploy
  image: gitlab-registry.cern.ch/cloud/ciadm
  script:
    - helm init --client-only
    - helm repo add cern https://registry.cern.ch/chartrepo/cern
    - helm repo update
    # helm-push not possible for now as it lacks --sign to pass a provenance file
    # - helm plugin install https://github.com/chartmuseum/helm-push
    - echo $HARBOR_SIGNKEY | base64 -d > secring.gpg
    - |
        for chart in $(ls -d */Chart.yaml | xargs dirname); do
            # Get local and remote versions
            LOCAL_VERSION=$(grep -R version ${chart}/Chart.yaml | awk '{print $2}')
            REMOTE_LATEST_VERSION=$(helm search cern/${chart} | grep cern/${chart} | awk '{print $2}')
            # Only push if chart version does not exists in remote
            if [ ${REMOTE_LATEST_VERSION} = "" ] || \
                [ $(expr ${REMOTE_LATEST_VERSION} \< ${LOCAL_VERSION}) -eq 1 ]; then
                helm dep update ${chart}
                helm package --sign --key registry --keyring secring.gpg ${chart}
                curl --fail -F "chart=@${chart}-${LOCAL_VERSION}.tgz" -F "prov=@${chart}-${LOCAL_VERSION}.tgz.prov" https://${HARBOR_USER}:${HARBOR_TOKEN}@registry.cern.ch/api/chartrepo/cern/charts
            fi
        done
  only:
    - tags
